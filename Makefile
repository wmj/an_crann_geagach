TEX_FILE := an_crann_geagach.tex
CHAPTERS := 1_cuireadh.tex \
           2_m_asal_beag_dubh.tex \
           3_croidhe-bhrughadh_na_cruinne.tex \
           4_sa_gcoill.tex \
           5_an_duiseacht.tex \
           6_an_bhean_bhi_ag_an_bhfuinneoig.tex \
           7_fleadh_aoibhinn.tex \
           8_beirt_faoi_chrann.tex \
           9_an_sean_choilear.tex \
           10_cnoc_mo_croidhe.tex \
           11_crann_na_croiche.tex \
           12_an_breac_ata_san_abhainn_mhoir.tex \
           13_slan_agaibh_a_chairde.tex \
           ap_claves_de_traduccion.tex \
           ap_licencia.tex \
           front.tex
LANGS := es ga

C_TEX := xelatex -interaction=nonstopmode
C_LATEXML := latexml
LATEXML_OPTS := --path=ltxml
LATEXMLC_OPTS := $(LATEXML_OPTS) --timeout=0
C_LATEXMLPOST := latexmlpost
C_LATEXMLC := latexmlc
C_EBOOK := ebook-convert
C_EBOOK_META := ebook-meta
EBOOK_CHAPTER := "//*[@class='ltx_chapter' or @class='ltx_appendix'] | //h:h6"
EBOOK_BREAK := "//*[@class='ltx_titlepage' or @class='ltx_role_newpage' or @class='ltx_role_contents' or @class='ltx_rule']"
EBOOK_OPTS := --verbose --chapter=$(EBOOK_CHAPTER) --page-breaks-before=$(EBOOK_BREAK)

HGTAG := $(shell hg log -r . --template "{latesttag}")

BASENAME := $(basename $(TEX_FILE))
VERNAME :=  $(BASENAME)_$(HGTAG)

LANG_TARGETS := $(addprefix $(VERNAME)_,$(LANGS))
XML_TARGETS :=  $(addsuffix .xml,$(LANG_TARGETS))
HTML_TARGETS := $(addsuffix .html,$(LANG_TARGETS))
EPUB_TARGETS := $(addsuffix .epub,$(LANG_TARGETS))
RAW_EPUB_TARGETS := $(addprefix raw_,$(EPUB_TARGETS))

.DEFAULT_GOAL := pdf

.PHONY: all pdf xml html epub
all: pdf epub

pdf:  $(VERNAME).pdf

xml:  $(XML_TARGETS)

html: $(HTML_TARGETS)

epub: $(EPUB_TARGETS)

# xelatex to pdf
$(VERNAME).pdf: $(TEX_FILE) $(CHAPTERS)
	$(C_TEX) -jobname=$(VERNAME) $<
	$(C_TEX) -jobname=$(VERNAME) $<

# xml for each language
$(VERNAME)_%.xml: $(TEX_FILE) ltxml/wmj.ltxml ltxml/side_%.ltxml $(CHAPTERS)
	$(C_LATEXML) $(LATEXML_OPTS) --preload=$(word 2,$^) --preload=$(word 3,$^) --destination=$@ $<

# html for each xml
$(HTML_TARGETS): $(VERNAME)_%.html: $(VERNAME)_%.xml
	$(C_LATEXMLPOST) --novalidate --destination=$@ $<

# raw_$(VERNAME)_%.epub: $(TEX_FILE) ltxml/wmj.ltxml ltxml/side_%.ltxml $(CHAPTERS)
# 	$(C_LATEXMLC) $(LATEXMLC_OPTS) --preload=$(word 2,$^) --preload=$(word 3,$^) --destination=$@ $<

# epub for each language
# $(EPUB_TARGETS): $(VERNAME)_%.epub: raw_$(VERNAME)_%.epub opf/%.opf
# 	cp $< $@
# 	$(C_EBOOK_META) $@ --from-opf=$(word 2,$^)

$(EPUB_TARGETS): $(VERNAME)_%.epub: $(VERNAME)_%.html opf/%.opf epub/estilo.css
	$(C_EBOOK) $< $@ --from-opf=$(word 2,$^) $(EBOOK_OPTS) --extra-css=$(word 3,$^)


# clean rules
#---------------

.PHONY: clean clean-aux clean-pdf clean-epub
clean: clean-aux clean-pdf clean-epub

clean-aux:
	-@rm -f *.dvi *.log *.ind *.aux *.toc *.syn *.idx *.out *.ilg *.pla *~

clean-pdf:
	-@rm -f *.pdf

clean-epub:
	-@rm -fr *.xml *.html *.css *.epub *.cache
