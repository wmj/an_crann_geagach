# An Crann Géagach - Pádraic Ó Conaire #

Edición bilingüe paralela irlandés-castellano.

He traducido este libro para aprender la lengua, así que puedes esperar que existan incorrecciones. Siendo así, cualquier aportación es bienvenida.

### Compilación ###

* Las fuentes están pensadas para ser compiladas con XeTex.
* Se ha usado la tipografía [Gadelica](http://www.iol.ie/~sob/gadelica/) para el texto en irlandés. Si prefieres no usar la tipografía tradicional, ha de desactivarse en el preámbulo del documento.

### pdf ###

Requiere `xelatex`.


```
#!bash
make

```

o

```
#!bash
make pdf
```

### html ###

Requiere [latexml](http://dlmf.nist.gov/LaTeXML/).

Se generan dos archivos, uno para cada lengua.


```
#!bash

make html
```

### epub ###

Requiere [calibre](http://calibre-ebook.com/) (en concreto la herramienta [ebook-convert](http://manual.calibre-ebook.com/cli/ebook-convert.html)). Se usa como entrada la conversión a html.

```
#!bash

make epub
```